Title: Vigsel

Vi kommer att hålla en borgerlig vigsel i anslutning till Aludden Prôvidore med start kl
16.00. Vi håller till utomhus på baksidan så länge vädret tillåter. Vid ev. regn kommer vi att
vara vid festlokalen. Aludden Prôvidore har öppet för allmänheten fram till vigseln så det
finns möjlighet till förströelse (och att släcka törsten) om man skulle anlända tidigt.

Bubbel med tilltugg serveras efter ceremonin vid festlokalen, därefter middag ca 17.30 och fest på samma ställe.

Klädsel: Kavaj

Men presenter då? Den bästa presenten är såklart att ni firar vår bröllopsdag tillsammans med
oss! Känner ni ändå att ni skulle vilja ge oss något skulle vi bli glada om ni då ger en gåva
till Greenpeace eller Unicef.