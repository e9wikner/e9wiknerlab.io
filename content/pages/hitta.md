Title: Hitta

Vigsel, middag och fest går alla av stapeln på Aludden Prôvidore, [Aspenäsvägen 12 i Lerum](https://www.aludden.se/kontakt/). Det ligger 20 km utanför Göteborg och man tar sig dit bekvämast med pendeltåg från Göteborgs centralstation. 

[Västtrafiks reseplanerare](https://www.vasttrafik.se/reseplanering/reseplaneraren/) guidar till *Aspedalen station* och man kan betala med deras [app](https://www.vasttrafik.se/biljetter/mer-om-biljetter/vasttrafik-to-go/) eller kreditkort på tåget, spårvagnen eller stombussen. Det är en enkelbiljett Göteborg-Lerum man behöver.

Ska man ta taxi är [Minitaxi](http://www.minitaxi.se/) billigast men dom tar tyvärr inte förbokningar. [Taxi Göteborg](https://www.taxigoteborg.se/Sv/Boka-taxi) rekommenderas annars.