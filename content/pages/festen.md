Title: Middag och fest

Efter vigseln sker mingel, bröllopsmiddag och fest på Aludden Prôvidore, som ligger i anslutning till vigselplatsen.

Vi önskar fira i vuxet sällskap utan barn. Små bebisar som ammas/tar flaska är självklart välkomna.

Våra vänner Sofia Klingberg och Robban Schmidt är toastmasters under middagen. Vill man anmäla tal eller andra upptåg kontaktar man dem på [toastmaster@anneliochstefan.se](mailto:toastmaster@anneliochstefan.se).

Ev. allergier/specialkost anmäls i samband med O.S.A.

Vi bjuder på fördrink, middagsdryck samt öl och vin i baren efter middagen.

Kl 02.00 stänger lokalen och då hinner man med pendeltåget mot Göteborg kl 02.36. Efterfest rekommenderas! :)