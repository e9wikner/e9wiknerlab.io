Title: fakta
Status: hidden

# Dagens sanning om våra kära gäster

**0.**  Kvällens brudgum och numera gift med 1.

**1.**  Kvällens brud och numera gift med 0.

**2.**  Med gröna fingrar har hon satt Föränge på lanthandelskartan. Hon odlar tresiffrigt antal gröna slangar varje år och delar generöst med sig till släkt och vänner. Gift med 3.

**3.**  Har du tur när du hälsar på får du följa med ut i garaget och kolla på hyveln. Där kan du även hitta Hälsinglands största samling av hammare. Gift med 2.

**4.**  Hittas ofta framför kartan i rallybilen eller på Grållen i Undersviks skogar. Brudens källa till glass, sladdar och hörlurar under uppväxten. Brudens far och sambo med 5.

**5.**  Hon tackar aldrig nej till en kopp kaffe och trivs som bäst vid en skogstjärn med fiskespö i handen. Sambo med 4, Torsten och Svea.

**6.**  Barnkär syster till bruden som förser Delsbo med mat och träningspepp. Regerande Undersviksmästare i rövkrok. Sambo med 7.

**7.**  Fd raggarbilsägare som gärna drar till fjälls med längdskidor och skotern på släp. Sambo med 6.

**8.**  Något av en expert på hall-innebandy som drog västerut och fann kärleken och det norska språket. Lovade bruden under uppväxten att han alltid skulle gilla Backstreet boys. Sambo med 9.

**9.**  Omtänksam norsk OS-expert med många systrar. Tackar förmodligen nej till en ostmacka men en Riesen kanske hon gillar. Sambo med 8.

**10.** Den yngre av brudens storasystrar som är fena på att sluka eld, blanda drinkar och att backa med släp. Tar det sällan lugnt när det kommer till sällskapsspel. Sambo med 11.

**11.** Gitarrspelande programmerare som bytt fokus från godis till glass. Bjuder han på öl, kolla procenten för säkerhetsskull. Sambo med 10.

**12.** Träningsintresserad syster till bruden med ett stort inredningsintresse. Är en fena på att dammsuga getingar men inte att förstå varför bruden åt kex på löpande band sommaren 2014. Gift med 13.

**13.** Drar upp kilovis med torsk i norska hav och kan allt om timmerhus. Ses ofta längs Ljusdals gator tidiga morgnar under vintertid. Gift med 12.

**14.** Vinterbadande Håkanfan som bäst trivs i en bastu eller med en bräda under fötterna. Använder gärna ordet "HÄRKE" och bangar inte en svartcamping. Kvällens toastmadame, gift med 15.

**15.** Äger en berömd morgonrock och fler elektriska färdmedel än de flesta. Beger sig till stranden så fort det blåser med sin fru kvällens toastmadame 14.

**16.** Brudens partner-in-crime under gymnasielektioner och på resande fot Down under. Har en svaghet för ostar och pudelrock. Gift med 17.

**17.** Antropologen som tillslut klippte sig och skaffade ett jobb. Gillar hallonpaj och hänger ofta på fjäll i Åre. Gift med 16.

**18.** Handbollsspelande hälsingestinta som tragglat såväl spanska verb som surfteknik tillsammans med bruden. Har tagit en springnota med stil. Gift med 19.

**19.** Bandyveteran som bytte Göteborg mot Söderhamn. Badar gärna i samband med bröllopsfest. Gift med 18 och tremänning med bruden.

**20.** En äkta skogsmulle som förser såväl tjejgänget som barnfamiljer med recept. La ribban gällande bröllopsparty när det begav sig 2007. Gift med 21.

**21.** Sportfantast som gärna springer långt och visslar på skolgården eller basketplanen. Huserade brudgummens svensexa i sin stuga och ser till så att alla har kissat innnan långpromenad. Gift med 20.

**22.** Musikalisk efterrättsmästarinna som går all-in när det vankas maskerad. Älskar hundar och det mesta med Australien. Sambo med 23.

**23.** Fotbollsfantast från Hisingen med hobbyn som yrke. Åker gärna på bilsemester till varmare breddgrader. Sambo med 22.

**24.** Dalkulla med stort matintresse som bytte stan mot schlätta. Har en fabless för skor och inreder gärna i vitt. Gift med 25.

**25.** Mannen med f kan allt om pellets och hittas ofta på en motorbåt i Värnen om somrarna. Gift med 24.

**26.** Schlagerfan som gärna bjuder på hembakat och primörer. Gillar bodypump och har nyss blivit med växthus hemma i Landvetter. Gift med 27.

**27.** Professor med ett förflutet inom buggvärlden. Träffas säkrast i Lysekil om somrarna. Gift med 26.

**28.** Brudens resekompanjon i Centralamerika som ofta hittas i stallet eller på yogamattan. Närvarade även vid den historiska kvällen på Skål. Sambo med 29.

**29.** Nybliven villaägare på Tjörn som gärna tar en sväng på mountainbike i skogen eller ett riff på gitarren. Sambo med 28.

**30.** Barnkär skidåkare som stickar ullkläder på löpande band. Besökte frekvent Göteborgs AW-ställen med bruden i början av 2000-talet.

**31.** Sambadansande yogi från Majorna som klämt åtskilliga pass med bodycombat och avsnitt Prison Break med bruden i Lidköping.

**33.** Salsadansande barndomsvän och resesällskap till bruden. Utgör också andra hälften av Agem-clan, numera på distans från Paris. Sambo med 34.

**34.** Long distance running martiniquean who enjoys cooking and has the french President among his previous employers. Living with 33.

**35.** Mamma till brugummen och Norrlands största Refreshments-fan. Öviks mest frekventa köksrenoverare som helst njuter av en Cava-Apperol på valfri solig ort. Sambo med 36.

**36.** Kolbullsgrillare som vet allt om välventilerade lokaler. Har ätit sin första och sista svartrot och dricker helst kaffe till kvällsfikat. Gillar livet på vägen dragandes på husvagn och fiske i sikte. Sambo med 35.

**37.** Pappa till brudgummen som njuter av livet som pensionär och att skjutsa på träning och match. Myntare av uttrycket ”HÖLLEDU” och familjen Wikners äldsta barn. En stor hockeyfantast som sällan missat en av MoDos  hemmamatcher. Gift med 38.

**38.** Lagar Öviks kanske godaste vårrullar och ser till att dom gamla äter gott varje dag. Gillar tennis och skidåkning men hejar aldrig på en Ottoson. Gift med 37 och Lukas mamma.

**39.** Farbor Lucas lärde sig åka skridskor innan han kunde gå och älskar både runda och platta bollar. Busar gärna med Melker och Hedda på Fejstajm. Vid 4 års ålder lärde han Anneli vad offside i hockey var. En riktig  sjusovare. Brudgummens bror.

**40.** Farbror Kicki har hittat padel på äldre dar och spelar fortfarande gubbhockey. Har bott i tantlägenhet och fått sin kära mor att gråta vid synen av flyttkartong som vardagsrumsbord. Brudgummens bror, särbo med 41.

**41.** En matälskare som jobbar sig sakta men säkert in på Hisingen. Hon är en stor favorit hos barnen och har aldrig hjärta att säga nej till lek med dom små. Särbo med 40.

**42.** Öviks modelejon som provat på livet som DJ, Svensson Svensson och hemsamarit. Familjens största 0-a i plump som till och med hävdar att han är bäst på julpusslet. Brudgummens bror, sambo med 43.

**43.** Tar hand om Öviks gamla och har lyckats med konststycket att göra man av lilla Jeppe. Hyser en hatkärlek till grekisk starkdryck och har ett odlarhjärta som värker efter ett växthus. Sambo med 42.

**46.** Barndomsvän till brudgummen som segrat i flest ätna megaskrov på Frasses men nuförtiden helst gnager på en tushi. Drömmer om livet på vägen i partybuss och skulle helst styra kosan till en match med Arsenal. Gift med 47.

**47.** Bredbytjejen som träffade Domsjöbo och hamnade på rätt sida älven. Inreder hemmet med fina detaljer och har en liten frisersalong hemma som kanske några av kvällens gäster nyligen besökt. Gift med 46.

**48.** Barndomsvän till brudgummen som hittat hem till Övik och industrin efter många år i huvudstan. En nostalgiker som kör med julprydnad året runt och alltid har träningskläderna packade på resan. Sambo med 49.

**49.** Nynästjej och lärare som hamnat i Foppaland på fel sida älven. Har sedan dess lärt sig sladda med handbroms, åka skidor och att gilla mat med tropisk frukt. Sambo med 48.

**50.** Undulatsovaren som levat i såväl kabyss, tält som på järnhäst med brudgummen. Den enda av de tre seglarna som gaddat sig med det förtjänta ankaret. Flitig besökare hos byggnadskontoret och Hornbachs faciliteter. Gift med 51.

**51.** Erfaren gast som lärt sig tala flytande norrländska under två svåra vintrar nära finska gränsen. Gillar snabba resultat och kanske därför tvättat BMW med gröna sidan in. Gift med 50.

**52.** Kvällens toastmaster lever helst livet till havs eller i skogen på två hjul. Han har lärt brudgumen allt om segling och kan alla 700 segelbåtar i test sovandes. Sambo med 53.

**53.** Landkrabba från Hindås som fick sig en seglare på kroken en dimmig kväll på en hård sten. Föredrar en blodig röd kotte framför en linswrap när laggen ska på. Sambo med 52.

**54.** Sjöscout som hängt på slottet och jagat ovala bollar på fotbollsplanen. Uppvuxen i Majorna men träffade en small-town-girl och lever numer på West Street. Kan allt om paraplyvinklar och har odlat både  lastbilsintresse och vattentomater tillsammans med brudgummen. Gift med 55 men vänstrar med Denise.

**55.** Entreprenören som vet allt om hur en uråldrig ek kapas bäst till ett maffigt matsalsbord. Uppvuxen vid små rullar papper och senare kunglig pappershovleverantör. Gift med 54.

**56.** Pastorn som visat brudgummen sin tacksamhet med fyra rosor från Kentucky. Är troligen iklädd vargpäls kvällen till ära men är det 30års-fest byter han fort om till mankini. Gift med 57.

**57.** Inbiten Majornabo som gillar Nepals klippiga landskap och dess uråldriga traditioner. En barista som bjuder på den godaste koppen och porslin att ta med. Gift med 56.

**58.** Mästaren på luftgitarr som sadlat om från grönställ till frihetsbranschen. Introducerade brudgummen till fjällvandring med en kylig tur kring Sylarna. Nu är det syllar, häst-transport och hockey som gäller. Gift med 59.

**59.** Äger en av Öviks större fordonsparker utan att riktigt vara medveten om det. Har kört plingplongdroska i Bredbyn men åker nu pendeln till björkarnas stad i forskningens tjänst. Gift med 58.

**60.** Växjös Wislander som bytte sjöarna mot indianerna och elektro på Chalmers. Trugas ej med tårta då den snabbt kan åka ut genom fönstret. Sambo med 61.

**61.** Arvikatjej som tidigare pushade gubbar i tights till max men nu är det vid fotbollsplanen heja-ropen hörs. Prutar hårdare än till och med brudgummens pappa och bor nära havet i liten by. Sambo med 60.

**62.** Koldioxidspecialist som gillar att studsa på vågorna lika väl som en 16 timmars kryss till Skagen så länge segelstället sitter rätt. Har hängt med brudgummen längs Brasiliens kust och nuförtiden i mountainbikespåren på Hisingen. Gift med 63.

**63.** Skövdetjejen som studerat i Jönköping och nu köper prylar åt femstjärnig båtfirma. Det är tack vare hennes inflyttningsfest och en hel del slumpen som vi firar bröllop idag. Gift med 62.

**64.** Rörtångsgrabben som nystartat ett lådbilsföretag men ibland anlitas av SPP för diverse hållfasthetsprover. Sprider skräck bland insekter på sina turer med tält och trivs med hantel i bur. Gift med 65.

**65.** Uppvuxen nära kexfabriken men trivs nu bäst i gamla hamnkvarter med dockor. Kan allt om pedagogik och hur man bäst kommunicerar utan varken hörsel eller syn. Gift med 64.

**66.** Ett dialektalt fenomen som rullar på r-en likt drottningen och ersätter ja-ordet likt norrlänningen. Har delat ett smurfigt kontor och jätteARG-skidåkning med brudgummen. Gift med 67.

**67.** Mariestadstjej som under många år jobbpendlat till Norge men nu lägger tiden på härliga vinterbad i Aspen. En festfixare som tar konceptet oktoberfest till nya nivåer. Gift med 66.

**68.** Tidigare Bäckedalsbo som gillar huskvarnar på hjul och vin med “penne”. Har tröttnat på solen längs kusten och letat sig in i skogarna kring Borås. Sambo med 69.

**69.** Uppvuxen i Elloskatalogens näste och spenderar gärna timmar bland second-handbutikernas brokiga klädhyllor. Lagar smaskens vegankrubb och har inspirerat till många härliga rätter. Sambo med 68.
